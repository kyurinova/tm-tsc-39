package ru.tsc.kyurinova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.endpoint.Session;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.endpoint.Task;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskIsBindToProjectByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-bind-to-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task to project by id...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();

        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectEndpoint().findByIdProject(session, projectId) == null)
            throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskEndpoint().findByIdTask(session, taskId) == null) throw new TaskNotFoundException();
        serviceLocator.getProjectTaskEndpoint().bindTaskById(session, projectId, taskId);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
