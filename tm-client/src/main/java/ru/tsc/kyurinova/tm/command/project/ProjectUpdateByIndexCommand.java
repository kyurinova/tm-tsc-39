package ru.tsc.kyurinova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractProjectCommand;
import ru.tsc.kyurinova.tm.endpoint.Session;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.endpoint.Project;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        if (!serviceLocator.getProjectEndpoint().existsByIndexProject(session, index))
            throw new ProjectNotFoundException();
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @NotNull final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().updateByIndexProject(session, index, name, description);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
