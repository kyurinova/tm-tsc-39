package ru.tsc.kyurinova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.5
 * 2022-03-05T18:50:28.710+03:00
 * Generated source version: 3.4.5
 */
@WebService(targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", name = "ProjectTaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectTaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/removeAllTaskByProjectIdRequest", output = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/removeAllTaskByProjectIdResponse")
    @RequestWrapper(localName = "removeAllTaskByProjectId", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.RemoveAllTaskByProjectId")
    @ResponseWrapper(localName = "removeAllTaskByProjectIdResponse", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.RemoveAllTaskByProjectIdResponse")
    public void removeAllTaskByProjectId(

            @WebParam(name = "session", targetNamespace = "")
                    ru.tsc.kyurinova.tm.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/unbindTaskByIdRequest", output = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/unbindTaskByIdResponse")
    @RequestWrapper(localName = "unbindTaskById", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.UnbindTaskById")
    @ResponseWrapper(localName = "unbindTaskByIdResponse", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.UnbindTaskByIdResponse")
    public void unbindTaskById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.tsc.kyurinova.tm.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId,
            @WebParam(name = "taskId", targetNamespace = "")
                    java.lang.String taskId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/findAllTaskByProjectIdRequest", output = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/findAllTaskByProjectIdResponse")
    @RequestWrapper(localName = "findAllTaskByProjectId", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.FindAllTaskByProjectId")
    @ResponseWrapper(localName = "findAllTaskByProjectIdResponse", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.FindAllTaskByProjectIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.tsc.kyurinova.tm.endpoint.Task> findAllTaskByProjectId(

            @WebParam(name = "session", targetNamespace = "")
                    ru.tsc.kyurinova.tm.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/removeByIdRequest", output = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/removeByIdResponse")
    @RequestWrapper(localName = "removeById", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.RemoveById")
    @ResponseWrapper(localName = "removeByIdResponse", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.RemoveByIdResponse")
    public void removeById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.tsc.kyurinova.tm.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/bindTaskByIdRequest", output = "http://endpoint.tm.kyurinova.tsc.ru/ProjectTaskEndpoint/bindTaskByIdResponse")
    @RequestWrapper(localName = "bindTaskById", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.BindTaskById")
    @ResponseWrapper(localName = "bindTaskByIdResponse", targetNamespace = "http://endpoint.tm.kyurinova.tsc.ru/", className = "ru.tsc.kyurinova.tm.endpoint.BindTaskByIdResponse")
    public void bindTaskById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.tsc.kyurinova.tm.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId,
            @WebParam(name = "taskId", targetNamespace = "")
                    java.lang.String taskId
    );
}
