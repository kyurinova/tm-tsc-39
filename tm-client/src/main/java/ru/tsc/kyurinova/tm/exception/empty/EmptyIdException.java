package ru.tsc.kyurinova.tm.exception.empty;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error. Id is empty.");
    }

    public EmptyIdException(String value) {
        super("Error." + value + " Id is empty.");
    }

}
