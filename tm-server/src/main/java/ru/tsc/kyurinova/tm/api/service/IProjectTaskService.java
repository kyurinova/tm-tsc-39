package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface IProjectTaskService {

    void bindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws SQLException;

    void unbindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws SQLException;

    void removeAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) throws SQLException;

    void removeById(@Nullable String userId, @Nullable String projectId) throws SQLException;

    @NotNull
    List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) throws SQLException;

}
