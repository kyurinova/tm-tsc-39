package ru.tsc.kyurinova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.exception.user.EmailExistsException;
import ru.tsc.kyurinova.tm.exception.user.LoginExistsException;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.util.HashUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService,
            @NotNull final IPropertyService propertyService
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
        this.propertyService = propertyService;
    }

    @Override
    public void addAll(@NotNull final List<User> users) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            for (User user : users) {
                userRepository.add(user);
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }


    @Nullable
    @Override
    public User findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable User user;
        try {
            user = userRepository.findByLogin(login);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable User user;
        try {
            user = userRepository.findByEmail(email);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public void isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = userRepository.findByLogin(login);
            if (user != null) throw new LoginExistsException();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = userRepository.findByEmail(email);
            if (user != null) throw new EmailExistsException();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.removeByLogin(login);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        isLoginExists(login);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        isLoginExists(login);
        isEmailExists(email);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        isLoginExists(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new EmptyUserIdException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.setPassword(userId, password);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.updateUser(userId, firstName, lastName, middleName);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("Y");
        try {
            userRepository.setLockedByLogin(login, "Y");
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("N");
        try {
            userRepository.setLockedByLogin(login, "N");
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public void remove(@Nullable final User entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.remove(entity);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findAll();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<User> findAll(final Comparator<User> comparator) {
        if (comparator == null) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findAllComporator(comparator);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findById(id);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public User findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findByIndex(index);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.removeByIndex(index);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return (userRepository.findById(id) != null);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return (userRepository.findByIndex(index) != null);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.getSize();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            sqlSession.close();
        }
    }
}
