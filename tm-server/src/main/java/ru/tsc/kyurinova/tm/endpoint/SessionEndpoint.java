package ru.tsc.kyurinova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.api.endpoint.ISessionEndpoint;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    private ISessionService sessionService;

    public SessionEndpoint(final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Session openSession(
            @NotNull
            @WebParam(name = "login", partName = "login")
                    String login,
            @NotNull
            @WebParam(name = "password", partName = "password")
                    String password
    ) {
        return sessionService.open(login, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void closeSession(
            @NotNull
            @WebParam(name = "session", partName = "session")
                    Session session
    ) {
        sessionService.close(session);
    }

}
