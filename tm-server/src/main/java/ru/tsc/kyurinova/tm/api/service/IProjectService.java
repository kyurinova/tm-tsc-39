package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectService {

    void addAll(@NotNull List<Project> projects);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project findByName(@Nullable String userId, @Nullable String name);

    void removeByName(@Nullable String userId, @Nullable String name);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    void startById(@Nullable String userId, @Nullable String id);

    void startByIndex(@Nullable String userId, @Nullable Integer index);

    void startByName(@Nullable String userId, @Nullable String name);

    void finishById(@Nullable String userId, @Nullable String id);

    void finishByIndex(@Nullable String userId, @Nullable Integer index);

    void finishByName(@Nullable String userId, @Nullable String name);

    void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

    void remove(@Nullable String userId, @Nullable Project project);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable String comparator);

    void clear();

    void clear(@Nullable String userId);

    @Nullable
    Project findById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project findByIndex(@Nullable String userId, @Nullable Integer index);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    boolean existsById(@Nullable String userId, @Nullable String id);

    boolean existsByIndex(@Nullable String userId, @NotNull Integer index);
}
