package ru.tsc.kyurinova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kyurinova.tm.api.service.IProjectTaskService;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.service.ProjectTaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectTaskEndpoint implements IProjectTaskEndpoint {

    private ISessionService sessionService;

    private IProjectTaskService projectTaskService;

    public ProjectTaskEndpoint(ISessionService sessionService, IProjectTaskService projectTaskService) {
        this.sessionService = sessionService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void bindTaskById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        sessionService.validate(session);
        projectTaskService.bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void unbindTaskById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        sessionService.validate(session);
        projectTaskService.unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @NotNull
    @WebMethod
    @SneakyThrows
    public void removeAllTaskByProjectId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        sessionService.validate(session);
        projectTaskService.removeAllTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        sessionService.validate(session);
        projectTaskService.removeById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull List<Task> findAllTaskByProjectId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        sessionService.validate(session);
        return projectTaskService.findAllTaskByProjectId(session.getUserId(), projectId);
    }
}
