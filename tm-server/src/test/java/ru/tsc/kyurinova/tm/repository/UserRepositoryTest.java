package ru.tsc.kyurinova.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.service.ConnectionService;
import ru.tsc.kyurinova.tm.service.PropertyService;
import ru.tsc.kyurinova.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class UserRepositoryTest {

    @NotNull
    private final SqlSession sqlSession;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "userTest@mail.ru";

    public UserRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
        userRepository = sqlSession.getMapper(IUserRepository.class);
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash(HashUtil.salt("testUser", 3, "test"));
    }

    @Before
    public void before() {
        userRepository.add(user);
        sqlSession.commit();
    }

    @Test
    public void findByUserTest() {
        Assert.assertEquals(user.getId(), userRepository.findById(userId).getId());
        Assert.assertEquals(user.getId(), userRepository.findByIndex(0).getId());
        Assert.assertEquals(user.getId(), userRepository.findByLogin(userLogin).getId());
        Assert.assertEquals(user.getId(), userRepository.findByEmail(userEmail).getId());
    }

    @Test
    public void removeUserTest() {
        Assert.assertNotNull(user);
        userRepository.remove(user);
        sqlSession.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByLoginTest() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(userLogin);
        userRepository.removeByLogin(userLogin);
        sqlSession.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByIdTest() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(userId);
        userRepository.removeById(userId);
        sqlSession.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByIdIndex() {
        Assert.assertNotNull(user);
        userRepository.removeByIndex(0);
        sqlSession.commit();
        Assert.assertNull(userRepository.findByIndex(0));
    }

    @After
    public void after() {
        userRepository.clear();
        sqlSession.commit();
        sqlSession.close();
    }

}
