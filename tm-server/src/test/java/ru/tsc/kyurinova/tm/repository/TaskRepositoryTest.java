package ru.tsc.kyurinova.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.api.repository.ITaskRepository;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.service.ConnectionService;
import ru.tsc.kyurinova.tm.service.PropertyService;
import ru.tsc.kyurinova.tm.util.HashUtil;

import java.sql.Timestamp;

public class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Task task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final static String PROJECT_NAME = "testProject";

    @NotNull
    private final String userId;

    @NotNull
    private final SqlSession sqlSession;

    public TaskRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
        taskRepository = sqlSession.getMapper(ITaskRepository.class);
        userRepository = sqlSession.getMapper(IUserRepository.class);
        projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final User user = new User();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 3, "test"));
        userRepository.add(user);
        task = new Task();
        sqlSession.commit();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setCreated(new Timestamp(task.getCreated().getTime()));
        project = new Project();
        sqlSession.commit();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(PROJECT_NAME);
        project.setCreated(new Timestamp(project.getCreated().getTime()));
        sqlSession.commit();
    }

    @Before
    public void before() {
        taskRepository.add(userId, task);
        projectRepository.add(userId, project);
        sqlSession.commit();
    }

    @Test
    public void findTaskTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertEquals(task.getId(), taskRepository.findById(userId, taskId).getId());
        Assert.assertEquals(task.getId(), taskRepository.findByIndex(userId, 0).getId());
        Assert.assertEquals(task.getId(), taskRepository.findByName(userId, taskName).getId());
    }

    @Test
    public void existsTaskTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertEquals(task.getId(), taskRepository.findById(userId, taskId).getId());
    }

    @Test
    public void removeTaskByIdTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.removeById(userId, taskId);
        sqlSession.commit();
        Assert.assertTrue(taskRepository.findAllUserId(userId).isEmpty());
    }

    @Test
    public void removeTaskByNameTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.removeByName(userId, taskName);
        sqlSession.commit();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void startByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.startById(userId, taskId);
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void startByIndexTest() {
        Assert.assertNotNull(userId);
        taskRepository.startByIndex(userId, 0);
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.startByName(userId, taskName);
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void finishByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.finishById(userId, taskId);
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void finishByIndexTest() {
        Assert.assertNotNull(userId);
        taskRepository.finishByIndex(userId, 0);
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.finishByName(userId, taskName);
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.changeStatusById(userId, taskId, Status.IN_PROGRESS);
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
        taskRepository.changeStatusById(userId, taskId, Status.COMPLETED);
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
        taskRepository.changeStatusById(userId, taskId, Status.NOT_STARTED);
        sqlSession.commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() {
        Assert.assertNotNull(userId);
        taskRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
        taskRepository.changeStatusByIndex(userId, 0, Status.COMPLETED);
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
        taskRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        sqlSession.commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.changeStatusByName(userId, taskName, Status.IN_PROGRESS);
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
        taskRepository.changeStatusByName(userId, taskName, Status.COMPLETED);
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
        taskRepository.changeStatusByName(userId, taskName, Status.NOT_STARTED);
        sqlSession.commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void findByProjectAndTaskIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        Assert.assertEquals(task.getId(), taskRepository.findByProjectAndTaskId(userId, projectId, taskId).getId());
    }

    @Test
    public void findAllTaskByProjectIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        Assert.assertEquals(task.getId(), taskRepository.findAllTaskByProjectId(userId, projectId).get(0).getId());
    }

    @Test
    public void bindTaskToProjectByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void unbindTaskByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
        taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        Assert.assertNull(taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void removeAllTaskByProjectIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        @NotNull final Task task1 = new Task();
        task1.setUserId(userId);
        taskRepository.add(userId, task1);
        taskRepository.bindTaskToProjectById(userId, projectId, task1.getId());
        sqlSession.commit();
        Assert.assertEquals(2, taskRepository.findAllTaskByProjectId(userId, projectId).size());
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        sqlSession.commit();
        Assert.assertEquals(0, taskRepository.findAllTaskByProjectId(userId, projectId).size());
    }

    @After
    public void after() {
        taskRepository.clear();
        sqlSession.commit();
        projectRepository.clear();
        sqlSession.commit();
        userRepository.clear();
        sqlSession.commit();
        sqlSession.close();
    }

}
