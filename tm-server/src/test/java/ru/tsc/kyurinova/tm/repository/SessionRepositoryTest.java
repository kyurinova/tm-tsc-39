package ru.tsc.kyurinova.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.service.ConnectionService;
import ru.tsc.kyurinova.tm.service.PropertyService;
import ru.tsc.kyurinova.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class SessionRepositoryTest {

    @NotNull
    private final SqlSession sqlSession;

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Session session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final String userId;

    public SessionRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
        userRepository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final User user = new User();
        user.setLogin("test");
        userId = user.getId();
        @NotNull final String password = "test";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
        userRepository.add(user);
        sqlSession.commit();
        sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        session = new Session();
        sessionId = session.getId();
        session.setUserId(userId);
        session.setTimestamp(System.currentTimeMillis());
    }

    @Before
    public void before() {
    }

    @Test
    public void existsSessionTest() {
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.findById(sessionId));
    }

    @Test
    public void removeSessionByIdTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(sessionId);
        sessionRepository.removeById(sessionId);
        sqlSession.commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        sessionRepository.clear();
        sqlSession.commit();
        sqlSession.close();
    }

}
